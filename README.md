# SBAB frontend case

## Introduction

SBAB frontend case is a pure React single page. More details regarding required features are available in the [Features](#Features) section below.

### 🏠 [Homepage](https://gitlab.com/prvate/monty-hall)

## Prerequisites

- TypeScript
- React
- Jest
- Tailwind

## Features

Application should be able to:

• Present the user with 3 doors, one of which contains a prize
• Allow the user to select a door
• Open one of the remaining unselected doors NOT containing a prize
• Allow the user to OPTIONALLY change their door selection
• Present if the user’s final door selection contains a prize

## For Development

### Install

- Clone this project to your local environment
- Run

```sh
npm install

```

### Usage

Start the page on [localhost:5173/](http://localhost:5173//).

```sh
npm run dev
```

### Testing

This project holds unit tests with Jest and React Testing Library.

| Command             | description        |
| ------------------- | ------------------ |
| `npm run test     ` | run all unit tests |
