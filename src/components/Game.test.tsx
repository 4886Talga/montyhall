import { render, fireEvent } from "@testing-library/react";
import Game from "./Game";

describe("Game", () => {
  test("renders without crashing", () => {
    render(<Game />);
  });

  test("shuffles doors when 'Shuffle Doors' button is clicked", () => {
    const { getByText, getAllByText } = render(<Game />);
    const shuffleButton = getByText("Shuffle Doors");
    fireEvent.click(shuffleButton);

    // Get the content of all doors after shuffling
    const doorsAfterShuffle = getAllByText(/Door 1|Door 2|Door 3/).map(
      (door) => door.textContent
    );

    // Get the content of all doors before shuffling
    const doorsBeforeShuffle = ["Goat", "Goat", "Car"];

    // Assert that the content of doors has changed after shuffling
    expect(doorsAfterShuffle).not.toEqual(doorsBeforeShuffle);
  });

  test("selects a door when clicked", () => {
    const { getByText } = render(<Game />);
    const door1 = getByText("Door 1");
    fireEvent.click(door1);
    expect(door1).toHaveClass("border-4 border-slate-300");
  });

  test("switches doors when 'Switch' button is clicked", () => {
    const { getByText } = render(<Game />);
    const door1 = getByText("Door 1");
    fireEvent.click(door1);

    const switchButton = getByText("Switch");
    fireEvent.click(switchButton);
  });

  test("keeps selected door when 'Keep' button is clicked", () => {
    const { getByText } = render(<Game />);
    const door1 = getByText("Door 1");
    fireEvent.click(door1);

    const keepButton = getByText("Keep");
    fireEvent.click(keepButton);
  });
});
