import { useState } from "react";
import Door from "./Door";

const Game = () => {
  const initialDoors = [
    { id: 1, isChosen: false, isOpen: false, content: "Goat" },
    { id: 2, isChosen: false, isOpen: false, content: "Goat" },
    { id: 3, isChosen: false, isOpen: false, content: "Car" },
  ];

  const [doors, setDoors] = useState(initialDoors);
  const [isFinished, setIsFinished] = useState(false);
  const [message, setMessage] = useState("");

  const shuffle = () => {
    const result = Math.floor(Math.random() * Math.floor(3) + 1);
    const initialContent = ["Goat", "Goat", "Goat"];
    initialContent[result - 1] = "Car";
    const initialDoors = initialContent.map((content, index) => ({
      id: index + 1,
      isChosen: false,
      isOpen: false,
      content: content,
    }));
    setDoors(initialDoors);
    setIsFinished(false);
    setMessage("");
  };

  const handleDoorClick = (id: number) => {
    if (isFinished) return;

    const updatedDoors = [...doors];
    updatedDoors[id - 1].isChosen = true;

    const goats = updatedDoors.filter(
      (door) => door.content === "Goat" && !door.isChosen
    );
    const goatToShow = goats[Math.floor(Math.random() * goats.length)];

    updatedDoors.forEach((door) => {
      if (door.id === goatToShow.id) door.isOpen = true;
    });

    setMessage(`You've chosen Door ${id}. Do you want to switch or keep?`);
    setIsFinished(true);
  };

  const handleSwitch = () => {
    const chosenDoor = doors.find((door) => door.isChosen && !door.isOpen);
    if (!chosenDoor) return;

    const remainingDoors = doors.filter(
      (door) => !door.isChosen && !door.isOpen
    );
    const switchToDoor =
      remainingDoors[Math.floor(Math.random() * remainingDoors.length)];
    if (switchToDoor) {
      chosenDoor.isOpen = false;
      chosenDoor.isChosen = false;
      switchToDoor.isOpen = true;
      if (switchToDoor.content === "Car") {
        setMessage("Congratulations! You won the car!");
      } else {
        setMessage("Sorry, you got a goat.");
      }
    } else {
      setMessage("No other door to switch to.");
    }

    setIsFinished(true);
  };

  const handleKeep = () => {
    const chosenDoor = doors.find((door) => door.isChosen && !door.isOpen);
    if (!chosenDoor) return;

    chosenDoor.isOpen = true;
    if (chosenDoor.content === "Car") {
      setMessage("Congratulations! You won the car!");
    } else {
      setMessage("Sorry, you got a goat.");
    }

    setIsFinished(true);
  };

  return (
    <div className="flex flex-col items-center">
      <button
        className="bg-blue-500 text-white px-4 py-2 my-4 rounded-md"
        onClick={shuffle}
      >
        Shuffle Doors
      </button>
      <div className="flex justify-center">
        {doors.map((door) => (
          <Door
            key={door.id}
            id={door.id}
            isChosen={door.isChosen}
            isOpen={door.isOpen}
            content={door.content}
            onClick={() => handleDoorClick(door.id)}
          />
        ))}
      </div>
      <div className="text-xl mt-8">{message}</div>
      {isFinished && (
        <div className="flex mt-4">
          <button
            className=" bg-green-500 text-white px-4 py-2 mr-2 rounded-lg"
            onClick={handleSwitch}
          >
            Switch
          </button>
          <button
            className=" bg-red-500 text-white px-4 py-2 rounded-lg"
            onClick={handleKeep}
          >
            Keep
          </button>
        </div>
      )}
    </div>
  );
};

export default Game;
