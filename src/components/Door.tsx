type DoorProps = {
  id: number;
  isChosen: boolean;
  isOpen: boolean;
  content: string;
  onClick: () => void;
};

const Door = ({ id, isChosen, isOpen, content, onClick }: DoorProps) => {
  let doorClass =
    "bg-blue-500 px-4 py-6 m-2 text-white text-lg cursor-pointer flex justify-center items-center";
  if (isChosen && !isOpen) doorClass += " border-4 border-slate-300";
  if (isOpen) doorClass += " bg-gray-400 cursor-not-allowed";
  if (isOpen && content === "Car")
    doorClass += " bg-gray-400 cursor-not-allowed border-4 border-yellow-500";

  return (
    <div className={doorClass} onClick={onClick}>
      {isOpen ? content : "Door " + id}
    </div>
  );
};

export default Door;
