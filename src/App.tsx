import "./App.css";
import Game from "./components/Game";

function App() {
  return (
    <main className="flex flex-col justify-center items-center">
      <h1 className="text-3xl font-bold">Monty Hall</h1>
      <p className="mb-4">Pick your door!</p>
      <Game />
    </main>
  );
}

export default App;
